
exports.up = async (knex, Promise) => {
  await knex.schema.createTable('profile', (table) => {
    table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
    table.uuid('user_id').references('users.id').unique().notNullable().onDelete('CASCADE')
    table.date('birth_date')
    table.string('gender')
    table.string('phone')
    table.text('about')
    table.timestamps(true, true)
  })
}

exports.down = async (knex, Promise) => {
  return knex.schema.dropTable('profile')
}
