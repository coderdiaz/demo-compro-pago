
exports.up = async (knex, Promise) => {
  await knex.schema.createTable('users', (table) => {
    table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
    table.string('email').unique().notNullable()
    table.string('username').notNullable()
    table.string('password').notNullable()
    table.string('first_name').notNullable()
    table.string('last_name').notNullable()
    table.timestamps(true, true)
  })
}

exports.down = async (knex, Promise) => {
  return knex.schema.dropTable('users')
}
