const bcrypt = require('bcrypt')

exports.seed = (knex, Promise) => {
  return knex('users').del()
    .then(async () => {
      let hashed = await bcrypt.hash('demo', 10)
      return knex('users').returning('id').insert([
        {
          email: 'coderdiaz@gmail.com',
          password: hashed,
          username: 'coderdiaz',
          first_name: 'Javier',
          last_name: 'Diaz Chamorro'
        }
      ]).then((id) => {
        return knex('profile').insert([
          {
            user_id: id[0]
          }
        ])
      })
    })
}
