'use strict'

module.exports = {
  development: {
    client: 'pg',
    connection: {
      database: 'demo_compro_pago'
    },
    migrations: {
      directory: 'database/migrations',
      tableName: 'migration'
    },
    seeds: {
      directory: 'database/seeds'
    }
  },
  production: {
    client: 'pg',
    connection: process.env.DATABASE_URL,
    migrations: {
      directory: 'database/migrations',
      tableName: 'migration'
    },
    seeds: {
      directory: 'database/seeds'
    }
  }
}