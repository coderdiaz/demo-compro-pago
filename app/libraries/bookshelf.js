'use strict'

import Bookshelf from 'bookshelf'
import { knex } from './knex'

export const bookshelf = Bookshelf(knex)

bookshelf.plugin(['bookshelf-camelcase', 'visibility', 'virtuals'])