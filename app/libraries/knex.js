'use strict'

import path from 'path'
import Knex from 'knex'
import KnexLogger from 'knex-tiny-logger'

const knexfile = require(path.resolve('./knexfile'))
const environment = process.env.NODE_ENV || 'development'

export const knex = Knex(knexfile[environment])

if (environment !== 'production') {
  KnexLogger(knex)
}
