import glob from 'glob'
import path from 'path'
import { Server } from 'hapi'
import { JWTAuth } from './jwt-auth'

export const server = new Server()

server.connection({
  port: process.env.PORT || 4000,
  host: '0.0.0.0',
  routes: {
    cors: {
      origin: ['*']
    }
  }
})

server.auth.scheme('JWTAuth', JWTAuth)
server.auth.strategy('jwt', 'JWTAuth')

server.route(glob.sync(path.join(__dirname, '../**/routes/*.js')).map(require))
