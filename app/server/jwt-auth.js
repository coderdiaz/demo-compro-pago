'use strict'

import { unauthorized } from 'boom'
import parseBearer from 'parse-bearer-token'
import jwt from 'jsonwebtoken'
import { User } from './../features/user'

export function JWTAuth () {
  return {
    async authenticate (request, reply) {
      const authorization = parseBearer(request)
      
      let user, token
      
      if (!authorization) {
        return reply(unauthorized())
      }

      try {
        token = jwt.verify(authorization, process.env.JWT_SECRET)
      } catch (err) {
        return replay(unauthorized())
      }

      try {
        user = await User.forge({ id: token.sub }).fetch()
      } catch (ex) {
        return reply(unauthorized())
      }

      if (!user) {
        return reply(unauthorized())
      }

      reply.continue({
        credentials: { user }
      })
    }
  }
}