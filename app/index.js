'use strict'

require('dotenv').config()
require('app-module-path').addPath(__dirname)

import { server } from './server'

server.start(err => {
  if (err) {
    throw err
  }

  console.log(`Running at: ${server.info.uri}`)
})
