'use strict'

import { User } from '../../user/models/user'
import { internal } from 'boom'

export default {
  method: 'GET',
  path: '/api/v1/users/{username}/profile',
  config: {
    auth: {
      strategy: 'jwt',
      mode: 'required'
    }
  },
  async handler (request, reply) {
    const userName = encodeURIComponent(request.params.username)
    await User.where('username', userName).fetch({
      withRelated: 'profile'
    }).then((user) => {
      reply({
        data: user
      })
    }).catch((err) => reply(internal()))
  }
}