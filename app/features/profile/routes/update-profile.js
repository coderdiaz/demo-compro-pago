'use strict'

import { Profile } from '../../profile/models/profile'
import { User } from '../../user/models/user'
import { badRequest, internal } from 'boom'

export default {
  method: 'PUT',
  path: '/api/v1/users/{username}/profile',
  config: {
    auth: {
      strategy: 'jwt',
      mode: 'required'
    }
  },
  async handler (request, reply) {
    let item = {
      birth_date: (request.payload.birthDate) ? request.payload.birthDate : '',
      gender: (request.payload.gender) ? request.payload.gender : '',
      phone: (request.payload.phone) ? request.payload.phone : '',
      about: (request.payload.about) ? request.payload.about : ''
    }
    const userName = encodeURIComponent(request.params.username)
    await User.where('username', userName).fetch().then((user) => {
      if (user) {
        if (item) {
          Profile.where('user_id', user.id)
          .save(item, { patch: true })
          .then((profile) => {
            reply({
              data: profile
            })
          }).catch(err => console.log(err))
        } else {
          reply(badRequest('Nothing for update'))
        }
      } else {
        reply(badRequest('The user is not found'))
      }
    }).catch(err => reply(internal(err)))
  }
}