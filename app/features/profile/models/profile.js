'use strict'

import { bookshelf } from '../../../libraries/bookshelf'

export const Profile = bookshelf.Model.extend({
  tableName: 'profile',
  hasTimestamps: ['createdAt', 'updatedAt']
})