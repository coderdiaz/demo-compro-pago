'use strict'

import { User } from '../../user/models/user'
import { unauthorized, badRequest, notFound } from 'boom'
import bcrypt from 'bcrypt'

export default {
  method: 'POST',
  path: '/api/v1/login',
  async handler (request, reply) {
    if (!request.payload.email || !request.payload.password) reply(badRequest('The email and password are required'))

    let email = request.payload.email
    let password = request.payload.password

    await User.where('email', email).fetch().then((user) => {
      if (user) {
        user.matchPassword(password).then((valid) => {
          if (valid) {
            let token = user.createJwtToken()
            reply({
              user: user.get('id'),
              token: token
            })
          } else {
            reply(unauthorized())
          }
        })
      } else {
        reply(unauthorized())
      }
    }).catch(err => {
      console.error(err)
    })
  }
}