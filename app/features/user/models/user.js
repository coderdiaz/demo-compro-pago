'use strict'

import bcrypt from 'bcrypt'
import uuid from 'uuid'
import jwt from 'jsonwebtoken'
import { bookshelf } from '../../../libraries/bookshelf'
import { Profile } from '../../profile'

export const User = bookshelf.Model.extend({
  tableName: 'users',
  
  hasTimestamps: ['createdAt', 'updatedAt'],

  hidden: ['password'],
  
  initialize () {
    this.on('saving', this.hashPassword)
  },

  profile () {
    return this.hasOne(Profile)
  },

  async hashPassword (user) {
    if (user.hasChanged('passsword')) {
      const password = user.get('password')
      const hashed = await bcrypt.hash(password, 10)
      user.set('password', hashed)
    }
  },

  matchPassword (password) {
    return bcrypt.compare(password, this.get('password'))
  },

  createJwtToken () {
    const sub = this.get('id')
    return jwt.sign({sub}, process.env.JWT_SECRET)
  },

  isSame (user) {
    return user.get('id') === this.get('id')
  }

})