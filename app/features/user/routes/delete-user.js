'use strict'

import { User } from '../../user/models/user'
import { internal, create } from 'boom'

export default {
  method: 'DELETE',
  path: '/api/v1/users/{username}',
  config: {
    auth: {
      strategy: 'jwt',
      mode: 'required'
    }
  },
  async handler (request, reply) {
    const userName = encodeURIComponent(request.params.username)
    await User.where('username', userName)
      .destroy()
      .then((user) => {
        reply().code(204)
      }).catch(err => console.log(err))
  }
}