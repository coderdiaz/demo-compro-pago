'use strict'

import { User } from '../../user/models/user'
import { Profile } from '../../profile/models/profile'
import { badRequest, internal } from 'boom'
import bcrypt from 'bcrypt'

export default {
  method: 'POST',
  path: '/api/v1/users',
  config: {
    auth: {
      strategy: 'jwt',
      mode: 'required'
    }
  },
  async handler (request, reply) {
    if (!request.payload.email) reply(badRequest('The email field is required'))
    if (!request.payload.username) reply(badRequest('The username is required'))
    if (!request.payload.password) reply(badRequest('The password field is required'))
    if (!request.payload.firstName) reply(badRequest('The first name field is required'))
    if (!request.payload.lastName) reply(badRequest('The last name field is required'))

    let passwordHashed = await bcrypt.hash(request.payload.password, 10)

    let item = {
      email: request.payload.email,
      username: request.payload.username,
      password: passwordHashed,
      first_name: request.payload.firstName,
      last_name: request.payload.lastName
    }

    User.where('email', item.email).fetch().then((user) => {
      if (user) {
        reply(badRequest('This email address already exists'))
      } else {
        User.where('username', item.username).fetch().then((user) => {
          if (user) {
            reply(badRequest('This username already exists'))
          } else {
            new User(item).save().then((user) => {
              new Profile({ user_id: user.attributes.id }).save().then((profile) => {
                reply({
                  data: user
                })
              })
            }).catch(err => reply(internal(err)))
          }
        })
      }
    }).catch(err => console.log(err))
  }
}