'use strict'

import { User } from '../../user/models/user'
import { badRequest, internal } from 'boom'

export default {
  method: 'PUT',
  path: '/api/v1/users/{username}',
  config: {
    auth: {
      strategy: 'jwt',
      mode: 'required'
    }
  },
  async handler (request, reply) {
    if (!request.payload.firstName) reply(badRequest('The first name field is required'))
    if (!request.payload.lastName) reply(badRequest('The last name field is required'))
    
    let firstName = request.payload.firstName
    let lastName = request.payload.lastName

    const userName = encodeURIComponent(request.params.username)
    await User.where('username', userName)
      .save({ first_name: firstName, last_name: lastName }, { patch: true })
      .then((user) => {
        reply({
          data: user
        })
      }).catch(err => reply(internal(err)))
  }
}