'use strict'

import { notFound, internal } from 'boom'
import { User } from '../../user/models/user'

export default {
  method: 'GET',
  path: '/api/v1/users/{username}',
  config: {
    auth: {
      strategy: 'jwt',
      mode: 'required'
    }
  },
  async handler (request, reply) {
    const userName = encodeURIComponent(request.params.username)
    await User.where('username', userName).fetch().then((user) => {
      if (user) {
        reply({
          data: user
        })
      } else {
        reply(notFound('The resource is not found'))
      }
    }).catch(err => console.error(err))
  }
}