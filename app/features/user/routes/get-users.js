'use strict'

import { User } from '../../user/models/user'

export default {
  method: 'GET',
  path: '/api/v1/users',
  config: {
    auth: {
      strategy: 'jwt',
      mode: 'required'
    }
  },
  async handler (request, reply) {
    await User
      .fetchAll()
      .then((users) => {
        reply({ data: users })
      })
  }
}